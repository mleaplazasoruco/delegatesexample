﻿namespace Delegate
{

    public class PhotoProcessor
    {
        public void Process (string path)
        {
            var photo = new Photo();
            var filters = new PhotoFilters();

            photo.Load(path);
            filters.ApplyBrightness(photo);
            filters.ApplyContrast(photo);

        }
    }
}
