﻿using System;

namespace Delegate
{
    
        public class PhotoFilters
        {
            public void ApplyBrightness (Photo photo)
            {
                Console.WriteLine("Brightness Filter");
            }
            public void ApplyContrast (Photo photo)
            {
                Console.WriteLine("Contrast");
            }
        }

}
