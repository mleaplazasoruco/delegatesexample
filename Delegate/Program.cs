﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Delegate
{
    partial class Program
    {
        static void Main(string[] args)
        {
            var processor = new PhotoProcessor();
            processor.Process("photo.jpg");
        }
    }
}
