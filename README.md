This is an Example of Delegates implementation on C#. 

Features:
    -Tightly coupled example
    -Basic Delegate implementation
    -Generic Delegate implementation

If you want to make a comparison between the different implementations you can checkout to the branches:
    -Delegate
    -GenericDelegate
    -NoDelegate

Happy Coding.